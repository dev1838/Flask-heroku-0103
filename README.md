# demo-cicd-flask-heroku

Python Flask Demo with Gitlab CI/CD and Heroku

## Blog Post:

Blog post about this is published here:
- https://blog.ruanbekker.com/blog/2019/01/05/tutorial-on-using-gitlab-cicd-pipelines-to-deploy-your-python-flask-restful-api-with-postgres-on-heroku/

## Resources:

- https://medium.com/python-pandemonium/build-simple-restful-api-with-python-and-flask-part-2-724ebf04d12
- https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563
- https://medium.com/@camillovisini/barebone-flask-rest-api-ac263db82e40
- https://pythonprogramming.net/password-hashing-flask-tutorial/

